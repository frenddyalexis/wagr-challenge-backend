'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Transaction.belongsTo(models.BetRequest, { foreignKey: 'betId' })
    }
  }
  Transaction.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      userId: DataTypes.CHAR(36),
      betId: DataTypes.INTEGER,
      amountAvailable: DataTypes.DECIMAL(10, 2)
    },
    {
      sequelize,
      modelName: 'Transaction'
    }
  )
  return Transaction
}
