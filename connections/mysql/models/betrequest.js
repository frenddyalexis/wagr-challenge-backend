'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class BetRequest extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BetRequest.hasMany(models.Transaction, { foreignKey: 'betId' })
      BetRequest.belongsTo(models.Bet, { foreignKey: 'matchId' })
    }
  }
  BetRequest.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      userId: DataTypes.CHAR(36),
      gameId: DataTypes.INTEGER,
      homeTeamId: DataTypes.INTEGER,
      awayTeamId: DataTypes.INTEGER,
      amount: DataTypes.DECIMAL(10, 2)
    },
    {
      sequelize,
      modelName: 'BetRequest',
      indexes: [{ unique: true, fields: ['userId', 'gameId', 'amount'] }]
    }
  )

  return BetRequest
}
