'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Bet extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Bet.hasMany(models.BetRequest, {
        as: 'betRequest',
        foreignKey: 'matchId'
      })
    }
  }
  Bet.init(
    {
      gameId: DataTypes.INTEGER,
      userA: DataTypes.CHAR(36),
      userATeam: DataTypes.INTEGER,
      userB: DataTypes.CHAR(36),
      userBTeam: DataTypes.INTEGER,
      amount: DataTypes.DECIMAL(10, 2)
    },
    {
      sequelize,
      modelName: 'Bet'
    }
  )
  return Bet
}
