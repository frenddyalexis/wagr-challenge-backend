import { createLogger, format, transports } from 'winston'
import dotenv from 'dotenv'

dotenv.config()

const ENABLED_LOG = process.env.ENABLED_LOG === 'true'

const getLabel = function (callingModule: { filename: string }) {
  var parts = callingModule.filename.split('/')
  return parts[parts.length - 2] + '/' + parts.pop()
}

export const logger = (module: any) =>
  createLogger({
    silent: !ENABLED_LOG,
    format: format.combine(
      format.simple(),
      format.timestamp(),
      format.label({
        label: getLabel(module),
        message: true
      }),
      format.colorize(),
      format.printf(
        (info) => `[${info.timestamp}] ${info.level} ${info.message} `
      )
    ),
    transports: [
      new transports.Console({
        level: 'debug'
      })
    ]
  })
