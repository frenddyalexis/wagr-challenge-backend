// const jwt = require('jsonwebtoken')
import jwt from 'jsonwebtoken'
import { logger } from '../utils/logger'

const log = logger(module)
const JWT_SECRET = process.env.JWT_SECRET || ''

export const decodedToken = (req: any, requireAuth = true): any => {
  {
    const header = req.req.headers.authorization

    log.info(`authenticating user....`)

    if (header) {
      const token = header.replace('Bearer ', '')
      const decoded = jwt.verify(token, JWT_SECRET)
      log.info(`authenticated user`)
      return decoded
    }

    if (requireAuth) {
      log.error(`authenticating error`)
      throw new Error('authenticating error')
    }

    return null
  }
}
