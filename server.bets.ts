import 'reflect-metadata'
import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import { buildSchema } from 'type-graphql'

/**
 * Resolvers
 */
import UserResolver from './services/user/user.resolver'
import GameResolver from './services/game/game.resolver'
import BetsResolver from './services/bet/bet.resolver'

import axios from 'axios'
import db from './connections/mysql/models'

const { GAMES_API, USERS_API } = process.env

const gamesApi = axios.create({
  baseURL: GAMES_API,
  timeout: 20000
})

const userApi = axios.create({
  baseURL: USERS_API,
  timeout: 20000
})

const app: express.Application = express()
const path = '/bets/graphql'
const PORT = process.env.PORT || 5000

const main = async () => {
  const schema = await buildSchema({
    resolvers: [UserResolver, GameResolver, BetsResolver],
    validate: false
  })

  // await db.sequelize.sync()

  // drop the table if it already exists
  await db.sequelize.sync({ force: true }).then(() => {
    console.log('Drop and re-sync db.')
  })

  const apolloServer = new ApolloServer({
    schema,
    introspection: true,
    playground: true,
    tracing: false,
    context: (req) => ({
      db,
      req,
      gamesApi,
      userApi
    })
  })

  apolloServer.applyMiddleware({ app, path })

  app.listen(PORT, () => {
    console.log(`🚀🚀started http://localhost:${PORT}${path}`)
  })
}

main()
