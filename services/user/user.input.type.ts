import { InputType, Field, ID, Int } from 'type-graphql'

@InputType({ description: 'Get User data' })
export default class GetUserInput {
  @Field(() => ID)
  userId: number
}
