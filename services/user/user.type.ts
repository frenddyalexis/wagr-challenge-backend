import { Field, ObjectType, ID, Int } from 'type-graphql'

@ObjectType()
export default class User {
  @Field(() => ID, { nullable: true })
  userId: number

  @Field()
  firstName: string

  @Field()
  lastName: string

  @Field()
  username: string

  @Field()
  balance: number
}
