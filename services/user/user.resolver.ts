import { Resolver, Query, Arg, Ctx } from 'type-graphql'

import GetUserInput from './user.input.type'
import UserResult from './user.results.type'

import { logger } from '../../utils/logger'
import User from './user.type'

import { sign } from 'jsonwebtoken'

const JWT_SECRET = process.env.JWT_SECRET || ''

const log = logger(module)

@Resolver()
export default class UserResolver {
  @Query(() => UserResult, {
    description: 'Get Users'
  })
  async user(
    @Arg('user')
    user: GetUserInput,
    @Ctx()
    ctx: any
  ): Promise<UserResult> {
    let result
    let totalUsers = 0
    let foundUser

    const { userApi } = ctx

    const { userId } = user

    try {
      log.info(`Getting users from ${userApi?.defaults?.baseURL}`)

      const getUser = await userApi.get('')
      result = getUser && getUser.data
      totalUsers = result && result.length

      log.info(`Found ${totalUsers} user`)

      if (result && result.length) {
        foundUser = result.filter((e: User) => e.userId == userId)

        const { userId: id, balance } = foundUser[0]
        if (foundUser && foundUser.length) {
          const dataJwt = {
            userId: id,
            initialBalance: balance
          }

          var tokenGenerate = sign(dataJwt, JWT_SECRET, {
            expiresIn: 7200 // expires in 24 hours
          })

          result = {
            access_token: tokenGenerate,
            user: foundUser,
            message: 'User Found'
          }
        }
      }
    } catch (err) {
      const error = err && err.message
      log.error(err)
      throw new Error(error)
    }

    return result
  }
}
