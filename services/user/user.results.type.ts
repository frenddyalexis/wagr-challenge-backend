import { ObjectType, Field } from 'type-graphql'
import User from './user.type'
@ObjectType()
export default class UserResult {
  @Field(() => String, { defaultValue: null })
  access_token: String

  @Field(() => String, { defaultValue: 'User Not Found' })
  message: String

  @Field(() => [User])
  user: User[]
}
