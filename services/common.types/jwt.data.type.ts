import { Field, ObjectType, ID, Int } from 'type-graphql'

@ObjectType()
export default class JwtData {
  @Field(() => ID)
  userId: number

  @Field()
  initialBalance: number

  @Field(() => Int)
  iat: number

  @Field(() => Int)
  exp: number
}
