import { Field, ObjectType, ID } from 'type-graphql'

@ObjectType()
export default class BaseFields {
  /**
   * BASE FIELDS
   */

  @Field(() => ID)
  id: number

  @Field()
  updatedAt: Date

  @Field()
  createdAt: Date
}
