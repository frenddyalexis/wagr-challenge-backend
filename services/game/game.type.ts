import { Field, ObjectType, ID, Int } from 'type-graphql'

@ObjectType()
export default class Game {
  @Field(() => ID)
  gameId: number

  @Field(() => ID)
  homeTeamId: number

  @Field(() => ID)
  awayTeamId: number

  @Field()
  startDateTime: string

  @Field()
  sport: string

  @Field(() => Int)
  homeRotationNumber: number

  @Field(() => Int)
  awayRotationNumber: number

  @Field(() => [Int], { nullable: true })
  quarters: []

  @Field({ nullable: true })
  currentHitter: string

  @Field({ nullable: true })
  currentPitcher: string

  @Field({ nullable: true })
  inningDescription: string

  @Field(() => [Int], { nullable: true })
  innings: []

  @Field(() => Int, { nullable: true })
  outs: number

  @Field(() => Int, { nullable: true })
  balls: number

  @Field(() => Int, { nullable: true })
  strikes: number

  @Field({ nullable: true })
  playingSurface: string

  @Field(() => Int, { nullable: true })
  capacity: number

  @Field(() => Int, { nullable: true })
  windSpeed: number

  @Field(() => Int, { nullable: true })
  windChill: number

  @Field({ nullable: true })
  channel: string
}
