import { ObjectType, Field, Int } from 'type-graphql'
import Game from './game.type'
@ObjectType()
export default class GameResults {
  @Field(() => [Game])
  items: Game[]

  @Field(() => Int)
  totalCount: number

  @Field()
  hasMore: boolean
}
