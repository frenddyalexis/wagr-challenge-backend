import { Resolver, Query, Args, Arg, Ctx } from 'type-graphql'

import GetGamesInput from './game.input.type'
import GameResults from './game.results.type'

import orderBy from 'lodash.orderby'

import { logger } from '../../utils/logger'
import Game from './game.type'

import { decodedToken } from '../../utils/decodedToken'

const log = logger(module)

@Resolver()
export default class GameResolver {
  @Query(() => GameResults, {
    description: 'Get Games available for betting'
  })
  async game(
    @Arg('game')
    game: GetGamesInput,
    @Ctx()
    ctx: any
  ): Promise<GameResults> {
    const { gameId, offset, order, limit } = game
    const { gamesApi, req } = ctx

    decodedToken(req)

    let result
    let totalGames = 0
    try {
      log.info(`Getting games from ${gamesApi?.defaults?.baseURL}`)

      const getGames = await gamesApi.get('')
      result = getGames && getGames.data
      totalGames = result && result.length

      log.info(`Found ${totalGames} games`)

      if (result && result.length) {
        if (gameId) {
          result = result.filter((e: Game) => e.gameId == gameId)
        } else {
          result = orderBy(result, ['startDateTime'], [order ? 'asc' : 'desc'])
        }
      }
    } catch (err) {
      const error = err && err.message
      log.error(err)
      throw new Error(error)
    }

    const hasMore = result.length > offset + limit

    return {
      items: result.slice(offset, offset + limit),
      totalCount: totalGames,
      hasMore: hasMore
    }
  }
}
