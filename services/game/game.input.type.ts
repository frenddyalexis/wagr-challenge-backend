import { InputType, Field, ID, Int } from 'type-graphql'

@InputType({ description: 'Input data for game query' })
export default class GetGamesInput {
  @Field(() => ID, { nullable: true })
  gameId: number

  @Field(() => Int, { defaultValue: 0 })
  offset: number

  // 1 -> asc 0 ->desc
  @Field(() => Boolean, { defaultValue: 1 })
  order: boolean

  @Field(() => Int, { defaultValue: 5 })
  limit: number
}
