import { Field, ObjectType, ID } from 'type-graphql'

@ObjectType()
export default class BetRequest {
  @Field(() => ID)
  gameId: number

  @Field({ nullable: true })
  homeTeamId: number

  @Field({ nullable: true })
  awayTeamId: number

  @Field()
  amount: number
}
