import { ObjectType, Field, Int } from 'type-graphql'

import BetsMatched from './bet.matched'
@ObjectType()
export default class BetsResults {
  @Field(() => [BetsMatched])
  items: BetsMatched[]

  @Field(() => Int)
  totalCount: number

  @Field()
  hasMore: boolean
}
