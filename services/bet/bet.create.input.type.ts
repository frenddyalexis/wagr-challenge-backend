import { InputType, Field, ID } from 'type-graphql'

@InputType({ description: 'Input data to create bets' })
export default class BetsCreateInput {
  @Field(() => ID)
  gameId: number

  @Field({ nullable: true })
  homeTeamId: number

  @Field({ nullable: true })
  awayTeamId: number

  @Field()
  amount: number
}
