import { Resolver, Query, Arg, Ctx, Mutation } from 'type-graphql'

import { Op } from 'sequelize'

import isEmpty from 'lodash.isempty'

import { logger } from '../../utils/logger'

import BetsCreateResults from './bet.create.results.type'
import BetsCreateInput from './bet.create.input.type'
import Game from '../game/game.type'
import { decodedToken } from '../../utils/decodedToken'
import JwtData from '../common.types/jwt.data.type'
import BetsResults from './bet.results.type'
import GetBetInput from './bet.input.type'

const log = logger(module)

@Resolver()
export default class BetsResolver {
  @Query(() => BetsResults, {
    description: 'Get  Bets'
  })
  async bet(
    @Arg('bet')
    bet: GetBetInput,
    @Ctx()
    ctx: any
  ): Promise<BetsResults> {
    const { offset, order, limit } = bet
    const { db, req } = ctx

    decodedToken(req)

    let result
    let totalMatched = 0
    /**
     * Models
     */
    const Bet = db.Bet
    const BetRequest = db.BetRequest
    try {
      log.info(`Getting bets whit input ${JSON.stringify(bet)}`)

      const getBet = await Bet.findAll({
        include: [
          {
            model: BetRequest,
            as: 'betRequest'
          }
        ],
        order: [['createdAt', order ? 'ASC' : 'DESC']]
      })

      result = getBet
      totalMatched = result && result.length

      log.info(`Found ${result.length} bet`)
    } catch (err) {
      const error = err && err.message
      log.error(err)
      throw new Error(error)
    }

    const hasMore = result.length > offset + limit

    return {
      items: result.slice(offset, offset + limit),
      totalCount: totalMatched,
      hasMore: hasMore
    }
  }
  @Mutation(() => BetsCreateResults, {
    description: 'Create Bet'
  })
  async createBet(
    @Arg('bet')
    bet: BetsCreateInput,
    @Ctx()
    ctx: any
  ): Promise<BetsCreateResults> {
    // params
    const { db, gamesApi, req } = ctx
    const { gameId, amount, homeTeamId, awayTeamId } = bet

    //user auth
    const jwtData: JwtData = decodedToken(req)
    const { userId, initialBalance } = jwtData

    //validate params
    if ((homeTeamId && awayTeamId) || (!homeTeamId && !awayTeamId)) {
      throw new Error('Must pass homeTeamId or awayTeamId')
    }

    //validate exist gaming
    const getGames = await gamesApi.get('')
    const getGamesResult = getGames && getGames.data
    const findGame = getGamesResult.filter((e: Game) => e.gameId == gameId)
    if (isEmpty(findGame)) {
      throw new Error('the game does not exist')
    }

    //validate exist homeTeamId
    if (homeTeamId) {
      const findHomeTeamId = findGame.filter(
        (e: Game) => e.homeTeamId == homeTeamId
      )

      if (isEmpty(findHomeTeamId)) {
        throw new Error('homeTeamId does not exist')
      }
    }
    //validate exist homeTeamId
    if (awayTeamId) {
      const findAwayTeamId = findGame.filter(
        (e: Game) => e.awayTeamId == awayTeamId
      )

      if (isEmpty(findAwayTeamId)) {
        throw new Error('awayTeamId does not exist')
      }
    }
    /**
     * Models
     */
    const BetRequest = db.BetRequest
    const Transaction = db.Transaction
    const Bet = db.Bet
    // init transaction
    const t1 = await db.sequelize.transaction()

    try {
      //validate amount
      const transactions = await Transaction.findAll({
        where: {
          userId: userId
        },
        order: [['createdAt', 'DESC']],
        attributes: ['amountAvailable'],
        limit: 1,
        raw: true
      })

      let amountAvailable
      if (isEmpty(transactions)) {
        amountAvailable = initialBalance - amount
      } else {
        const transactionAmountAvailable = transactions[0].amountAvailable
        amountAvailable = transactionAmountAvailable - amount
      }

      if (amountAvailable < 0) {
        throw new Error(`insufficient balance`)
      }

      const betParams = {
        userId,
        gameId,
        homeTeamId,
        awayTeamId,
        amount
      }

      log.info(`Creating bet with params ${JSON.stringify(betParams)}`)

      const betsResultCreate = await BetRequest.create(betParams, {
        transaction: t1
      })

      const { dataValues } = betsResultCreate

      const { id: betId } = dataValues

      const transactionParams = { userId, betId, amountAvailable }

      log.info(
        `Creating Transaction with params ${JSON.stringify(transactionParams)}`
      )

      await Transaction.create(transactionParams, { transaction: t1 })

      await t1.commit()

      //matching bet
      // init transaction
      const t2 = await db.sequelize.transaction()
      try {
        log.info(`looking for bets to match...`)
        //get bets unmatched
        let queryObject: any = {}
        queryObject.where = {}
        queryObject.where.userId = { [Op.notIn]: [userId] }
        queryObject.where.matchId = { [Op.is]: null }
        queryObject.where.amount = { [Op.eq]: amount }

        if (homeTeamId) {
          queryObject.where.homeTeamId = { [Op.is]: null }
        } else {
          queryObject.where.awayTeamId = { [Op.is]: null }
        }
        queryObject.order = [['createdAt', 'ASC']]
        queryObject.limit = 1
        queryObject.raw = true

        const betsResult = await BetRequest.findAll(queryObject)

        if (isEmpty(betsResult)) {
          log.info(`no matching bets found....`)
        } else {
          const {
            id: betIdSecond,
            userId: userA,
            homeTeamId: homeTeamIdResult,
            awayTeamId: awayTeamIdResult
          } = betsResult[0]
          log.info(`Bet to match was found.....`)
          const userB = userId
          const userATeam = homeTeamId ? homeTeamId : homeTeamIdResult
          const userBTeam = awayTeamId ? awayTeamId : awayTeamIdResult
          const matchedBetsParams = {
            gameId,
            userA,
            userATeam,
            userB,
            userBTeam,
            amount
          }
          log.info(
            `Creating record for MatchedBets with params ${JSON.stringify(
              matchedBetsParams
            )}`
          )
          const { id: matchedId } = await Bet.create(matchedBetsParams, {
            transaction: t2
          })

          const paramsUpdate = {
            matchId: parseInt(matchedId)
          }

          await BetRequest.update(paramsUpdate, {
            where: {
              id: { [Op.in]: [betId, betIdSecond] }
            },
            transaction: t2 //second parameter is "options", so transaction must be in it
          })
        }

        await t2.commit()
      } catch (err) {
        log.error(err)
        await t2.rollback()
      }

      return {
        bet: dataValues,
        message: 'Bet Created'
      }
    } catch (err) {
      log.error(err)
      await t1.rollback()
      throw new Error(err)
    }
  }
}
