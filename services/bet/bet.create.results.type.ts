import { ObjectType, Field, Int } from 'type-graphql'
import Bet from './bet.type'

@ObjectType()
export default class BetsCreateResults {
  @Field(() => Bet)
  bet: Bet

  @Field()
  message: string
}
