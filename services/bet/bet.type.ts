import { Field, ObjectType, ID } from 'type-graphql'
import BaseFields from '../common.types/base.fields.type'

@ObjectType()
export default class Bet extends BaseFields {
  @Field(() => ID)
  gameId: number

  @Field(() => ID)
  userId: number

  @Field()
  amount: number
}
