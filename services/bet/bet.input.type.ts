import { InputType, Field, ID, Int } from 'type-graphql'

@InputType({ description: 'Input data for betting query' })
export default class GetBetInput {
  @Field(() => Int, { defaultValue: 0 })
  offset: number

  @Field(() => Boolean, { defaultValue: 1 })
  order: boolean

  @Field(() => Int, { defaultValue: 5 })
  limit: number
}
