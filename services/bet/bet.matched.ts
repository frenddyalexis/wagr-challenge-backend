import { Field, ObjectType, ID, Int } from 'type-graphql'
import BaseFields from '../common.types/base.fields.type'

import BetRequest from './bet.request.type'

@ObjectType()
export default class BetsMatched extends BaseFields {
  @Field(() => ID)
  gameId: number

  @Field()
  userA: string

  @Field(() => Int, { defaultValue: 0 })
  userATeam: number

  @Field()
  userB: string

  @Field(() => Int, { defaultValue: 0 })
  userBTeam: number

  @Field()
  amount: number

  @Field(() => [BetRequest])
  betRequest: BetRequest[]
}
