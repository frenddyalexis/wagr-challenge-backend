# wagr-challenge-backend

# Introduction

The application is a Betting Engine that allows users to create open bets and automatically match them with other users taking the other side of the bet.

# Tech We Have Used

We are using node as main technology combined with express and mysql.
We use graphQL for endpoint queries and sequelize as ORM

## API

- Type GraphQL
- Type ORM
- mySQL

# Getting Started & Installation

`for dev mode`

**First you must create a local instance in mysql with a database called wagr**
**Second you must Create .env file with the same data as env.example**

1.clone repository
2.run `npm install`
3.run `npm run dev`

`for dev mode using docker`

**To test the integration with docker, you must register the IP in cloudSQL (google). Please contact me for this.**

1.clone repository
2.run `npm run docker:build`
3.run `npm run docker:up`

To stop the container

run `npm run docker:stop`

To delete the container

run `npm run docker:down`

Postman collection

`to test the endpoints with postman use the link below`

https://documenter.getpostman.com/view/71332/Tz5iBMaG
