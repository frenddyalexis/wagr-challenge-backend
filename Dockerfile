# The version of node that the application will use
FROM node:12.9.1

# Creating working directory where the application will we started
RUN mkdir /my_app

# Copy build typescript to directory where the application will we started
COPY dist /my_app

# Copy package.json to app 
COPY package.json /my_app

# The working directory where the application will we started
WORKDIR /my_app

# Install the dependencies
RUN npm install

# Exposed port for container
EXPOSE 4000

# Install PM2 to manage the application (https://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/)
RUN npm install pm2 -g

# The command to start the application using PM2
CMD ["pm2-runtime", "./server.bets.js"]





